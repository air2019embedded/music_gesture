from imutils.video import VideoStream
import datetime
import argparse
import imutils
import time
import handy
import alsaaudio
import pygame
import cv2
from Gesture import Gesture
# capture the hand histogram by placing your hand in the box shown and
# press 'A' to confirm
# source is set to inbuilt webcam by default. Pass source=1 to use an
# external camera.
hist = handy.capture_histogram(source=0)
cap = cv2.VideoCapture(0)
gesture=Gesture()
m = alsaaudio.Mixer('PCM')
pygame.mixer.init()
pygame.mixer.music.load("videoplayback.wav")
pygame.mixer.music.play()
while True:
    ret, frame = cap.read()
    if not ret:
        break
    hand = handy.detect_hand(frame, hist)

    # to get a quick outline of the hand
    quick_outline = hand.outline

    # draw fingertips on the outline of the hand, with radius 5 and color red,
    # filled in.
    for fingertip in hand.fingertips:
        cv2.circle(quick_outline, fingertip, 5, (0, 0, 255), -1)
    # to get the centre of mass of the hand
    com = hand.get_center_of_mass()
    if com:
        cv2.circle(quick_outline, com, 10, (255, 0, 0), -1)
        current_volume = m.getvolume() # Get the current Volume
        gesture.recognize(com,hand.fingertips)
        if gesture.direction == "one":
            cv2.putText(quick_outline, '1', (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
            pygame.mixer.music.pause()
        elif gesture.direction == "two":
            cv2.putText(quick_outline, '2', (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
            pygame.mixer.music.unpause()
        elif gesture.direction == "left":
            cv2.arrowedLine(quick_outline, (150,100), (50,100), (0,255,0),4)
            if(m.getvolume()[0] > 4):
                m.setvolume(int(m.getvolume()[0])-5)
        elif gesture.direction == "right":
            cv2.arrowedLine(quick_outline, (50,100), (150,100), (0,255,0),4)
            if(m.getvolume()[0] < 96):
                m.setvolume(int(m.getvolume()[0])+5)

    cv2.imshow("Handy", quick_outline)

    # display the unprocessed, segmented hand
    # cv2.imshow("Handy", hand.masked)

    # display the binary version of the hand
#    cv2.imshow("Handy", hand.binary)

    k = cv2.waitKey(5)

    # Press 'q' to exit
    if k == ord('q'):
        pygame.mixer.quit()
        break

cap.release()
cv2.destroyAllWindows()
