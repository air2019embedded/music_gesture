import cv2
import math
import numpy as np

class Gesture:

    def __init__(self):
        self.center_x = [0]*5
        self.center_y = [0]*5
        self.direction = "none"
    def recognize(self, point, fingers):
        if(fingers):    
            if(len(fingers)==5):
                self.center_x = np.roll(self.center_x,-1)
                self.center_x[4] = point[0]
                self.center_y = np.roll(self.center_y,-1)
                self.center_y[4] = point[1]
                if(((fingers[0][1] < (fingers[1][1] - 150)) or \
                    (fingers[0][0] > (fingers[1][0] - 50))) and \
                    fingers[0][1] < (fingers[2][1] - 100) and \
                    fingers[0][1] < (fingers[3][1] - 100) and \
                    fingers[0][1] < (fingers[4][1] - 150)):
                    self.direction = "one"
                elif(((fingers[0][1] < (fingers[2][1] - 100)) or \
                    (fingers[0][0] < (fingers[1][0] - 150))) and \
                    fingers[0][1] < (fingers[3][1] - 100) and \
                    fingers[0][1] < (fingers[4][1] - 150) and \
                    fingers[1][1] < (fingers[2][1] - 100) and \
                    fingers[1][1] < (fingers[3][1] - 100) and \
                    fingers[1][1] < (fingers[4][1] - 150)):
                    self.direction = "two"
                elif(self.center_x[4]-self.center_x[0])>50:
                    self.direction = "left"
                elif(self.center_x[0]-self.center_x[4])>50:
                    self.direction = "right"
                else:
                    self.direction = "none"
        print(self.direction)
        print(fingers[0])
        print(fingers[1])
 

